---
theme: gaia
size: 16:9
class:
  - invert
  - lead
paginate: true
backgroundColor: #000
backgroundImage: url('https://gitlab.com/lacoute974/iot/co2-sensor/yocto-presentation/-/raw/5c69047394500c6c9aed14ebc76029fe9259b913/img/background.jpg?inline=false')
---
# **Introduction to Yocto**

Create your own Linux distribution for embedded devices

Patrice Lacouture

---

```bash
$ whoami
Patrice Lacouture
```

![w:600](img/80s.png)

---

```bash
$ whoami
Patrice Lacouture
```

![w:600](img/gemalto.png)

---

```bash
$ whoami
Patrice Lacouture
```

![w:800](img/see.png)

---

# **What is Linux ?**

<span style="font-size:80%">

- Linux is *NOT* what you can see on the screen!
- Linux *ONLY* provides essential Operating System functions:
- schedule execution of "userland" processes 
- manage ressources
  - CPU(s)
  - memory
  - storage
  - network
  - peripherals

</span>

---
# **Linux system startup**

- 1- bootloader starts
- 2- bootloader loads **kernel**
- 3- kernel launches **init** process (PID 1)
- 4- init starts other **user processes**

---

# **What is a Linux *distribution* ?**

A simple way to get a working operating system:

<span style="font-size:70%">

- a configured bootloader
- a Linux kernel
- a filesystem with installed applications
- user accounts
- a package management system (RPM/DEB/Pacman/OPKG/...)
- a repository of available packages
- a graphical user interface (opt)
- customer support (opt)

- supporting a machine target (PC-X86 / Mac / Raspberry Pi / IBM Z / etc.)
</span>
---

# **What is a Linux distribution ?**

- Debian
- Ubuntu, flavours and derivatives
- Red Hat / CentOS / Fedora ...
- Suse / Opensuse
- Archlinux / Gentoo / ...
- Hannah Montana OS (!)

---

# **How are embedded devices different ?**

- Tailored to specific device
- Specific device peripherals
- Single use
- Constrained (RAM, Storage, connectivity)
- No traditional user account
- No package management
- No on-board development (cross-compiling)

---

# **Some popular tools to build embedded Linux systems**

- OpenWRT
- Buildroot
- Yocto
- (or classic Linux distributions)

---

# **OpenWRT**

- https://openwrt.org/
- Network devices, routers
- Command line + Web interface
- Classic distribution model
- Cross-compilation toolset
- OPKG package manager

---

# **Buildroot**

- https://buildroot.org
- lightweight, single-use devices
- Very small footprint
- Busybox shell and tools
- Full image installation and upgrade
- Typically no run-time package management
- Simple to use

---

# **Yocto**

- https://yoctoproject.org
- Single-use or versatile devices
- Small to medium footprint
- Large collection of available packages
- Generate your own distribution
- Optional run-time package management
- Generate SDK
- Complex!

---

# **Yocto: the general Workflow**

![h:500](img/yp-how-it-works-new-diagram-transparent.png)

---

# **Yocto components**

Build tool: bitbake

<span style="font-size:80%">

- Execute build operations:
  - Fetch – get the source code
  - Extract – unpack the sources
  - Patch – apply patches for bug fixes and new capability
  - Configure – set up your environment specifications
  - Build – compile and link
  - Install – copy files to target directories
  - Package – bundle files for installation

</span>

---

# **Yocto components**

Build tool: bitbake

- Manage dependencies
- Optimize build operations:
  - cache intermediate results
  - use SHA256 signatures
  - share caches

---

# **Yocto components**

Package description: Recipes

- Point to the package sources (Git, Hg, HTTP...)
- Describe build operations
- Describe distribution operations
  - create package
  - create device image
  - __install build tools on build host__

---

# **Yocto components**

Package collections: Layers

- Collections of package recipes
- Top layers can use definitions in lower layers
- Top layers can modify recipes in lower layers

---

# **Yocto components**

Package collections: Layers

https://layers.openembedded.org

- Poky: reference distribution
- openembedded-core: build tools, base libraries and tools
- meta-oe: additional recipes
- Board Support Packages and hardware specific layers:
  - meta-raspberrypi / meta-freescale
  - meta-ti

---

# **Yocto components**

Package collections: Layers

<span style="font-size:80%">

- Domain-specific layers: 
  - meta-networking
  - meta-audio
  - meta-go / meta-python / meta-rust ...
  - meta-lorawan
  - meta-ros
  - meta-neural-network
- Create your own !

</span>

---

<style scoped>
  section{justify-content: center;}
</style>

# **A practical example: CO2 datalogger**

- A Raspberry Pi
- A CO2 sensor

![bg right:50% w:40%](img/co2-sensor.jpg)

---

# **A practical example: CO2 datalogger**

- A selection of libraries and applications
- Custom programs
- User customization

---

# **Prepare build environment**

Build system:

- https://docs.yoctoproject.org/current/ref-manual/system-requirements.html#supported-linux-distributions
- Ubuntu Server 20.04 LTS virtual machine
- 10 vCPUs, 16 GB RAM, 70 GB storage
- (could be a Docker container)
- non-root user (mandatory!)

---

# **Prepare build environment**

Install Yocto requirements

<span style="font-size:80%">

- https://docs.yoctoproject.org/current/ref-manual/system-requirements.html#required-packages-for-the-build-host
```bash
$ sudo apt install gawk wget git diffstat unzip texinfo gcc \
                   build-essential chrpath socat cpio \
                   python3 python3-pip python3-pexpect \
                   xz-utils debianutils iputils-ping \
                   python3-git python3-jinja2 \
                   libegl1-mesa libsdl1.2-dev pylint3 \
                   xterm python3-subunit mesa-common-dev \
                   zstd liblz4-tool
```
</span>

---

# **Base system: Raspberry Pi 3**

![](img/raspi.jpg)

---

# **Create workspace**

We need 2 layers:

<span style="font-size:80%">

- `poky`: reference distribution
- `meta-raspberrypi`: https://meta-raspberrypi.readthedocs.io/en/latest/

`~/co2-sensor/0-bootstrap.sh`:
```bash
#!/bin/bash
BRANCH=honister

git clone -b $BRANCH git://git.yoctoproject.org/poky
git clone -b $BRANCH git://git.yoctoproject.org/meta-raspberrypi
```
(video 2-create-workspace.sh)
</span>

---

# **poky directory**

<span style="font-size:60%">

```
drwxrwxr-x 1 yocto yocto   228 Feb 23 21:35 bitbake
drwxrwxr-x 1 yocto yocto    32 Feb 23 21:35 contrib
drwxrwxr-x 1 yocto yocto   718 Feb 23 21:35 documentation
-rw-rw-r-- 1 yocto yocto   834 Feb 23 21:35 LICENSE
-rw-rw-r-- 1 yocto yocto 15394 Feb 23 21:35 LICENSE.GPL-2.0-only
-rw-rw-r-- 1 yocto yocto  1286 Feb 23 21:35 LICENSE.MIT
-rw-rw-r-- 1 yocto yocto  2202 Feb 23 21:35 MAINTAINERS.md
-rw-rw-r-- 1 yocto yocto  1222 Feb 23 21:35 Makefile
-rw-rw-r-- 1 yocto yocto   244 Feb 23 21:35 MEMORIAM
drwxrwxr-x 1 yocto yocto   466 Feb 23 21:35 meta
drwxrwxr-x 1 yocto yocto    74 Feb 23 21:35 meta-poky
drwxrwxr-x 1 yocto yocto   112 Feb 23 21:35 meta-selftest
drwxrwxr-x 1 yocto yocto   188 Feb 23 21:35 meta-skeleton
drwxrwxr-x 1 yocto yocto   138 Feb 23 21:35 meta-yocto-bsp
-rwxrwxr-x 1 yocto yocto  1293 Feb 23 21:35 oe-init-build-env
lrwxrwxrwx 1 yocto yocto    33 Feb 23 21:35 README.hardware.md -> meta-yocto-bsp/README.hardware.md
lrwxrwxrwx 1 yocto yocto    14 Feb 23 21:35 README.md -> README.poky.md
-rw-rw-r-- 1 yocto yocto   791 Feb 23 21:35 README.OE-Core.md
lrwxrwxrwx 1 yocto yocto    24 Feb 23 21:35 README.poky.md -> meta-poky/README.poky.md
-rw-rw-r-- 1 yocto yocto   529 Feb 23 21:35 README.qemu.md
drwxrwxr-x 1 yocto yocto  2440 Feb 23 21:35 scripts
```

</span>

---

# **Setup Yocto environment**

```bash
$ cd ~/co2-sensor
$ source poky/oe-init-build-env
```

(video 3-init-build-env.sh)

```
$ man source
```

---

# **build directory**

```
co2-sensor/build
└── conf
    ├── bblayers.conf
    ├── local.conf
    └── templateconf.cfg
```

---

# **Configure layers**

<span style="font-size:60%">

`~/co2-sensor/build/conf/bblayers.conf`:

```
# POKY_BBLAYERS_CONF_VERSION is increased each time build/conf/bblayers.conf
# changes incompatibly
POKY_BBLAYERS_CONF_VERSION = "2"

BBPATH = "${TOPDIR}"
BBFILES ?= ""

BBLAYERS ?= " \
  /home/yocto/co2-sensor/poky/meta \
  /home/yocto/co2-sensor/poky/meta-poky \
  /home/yocto/co2-sensor/poky/meta-yocto-bsp \
  /home/yocto/co2-sensor/meta-raspberrypi \
  "
```
</span>

---

# **Local configuration**

<span style="font-size:60%">

Lines added to `~/co2-sensor/build/conf/local.conf`:

```
MACHINE ??= "raspberrypi3"
GPU_MEM = "16"
ENABLE_UART = "1"
ENABLE_I2C = "1"
KERNEL_MODULE_AUTOLOAD:rpi += "i2c-dev i2c-bcm2708"
DISTRO_FEATURES:append = " systemd"
VIRTUAL-RUNTIME_init_manager = "systemd"
DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"
VIRTUAL-RUNTIME_initscripts = ""
```
</span>

https://meta-raspberrypi.readthedocs.io/en/latest/extra-build-config.html

<span style="font-size:50%">

*Nota bene:* following best practices, some of the definitions above should not be
included in this file, but in other configuration files or recipes. We include them
here for the sake of simplification.
</span>

---

# **Local configuration**

<span style="font-size:60%">

`~/co2-sensor/build/conf/local.conf`:

```
#DL_DIR ?= "${TOPDIR}/downloads"
#SSTATE_DIR ?= "${TOPDIR}/sstate-cache"
#TMPDIR = "${TOPDIR}/tmp"
```
</span>

https://docs.yoctoproject.org/current/ref-manual/variables.html

- `TMPDIR` is build host and project specific
- `DL_DIR` and `SSTATE_DIR` can be shared!

---

# **Build a target**

<span style="font-size:60%">

```bash
$ source poky/oe-init-build-env
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal
    core-image-full-cmdline
    core-image-sato
    core-image-weston
    meta-toolchain
    meta-ide-support

You can also run generated qemu images with a command like 'runqemu qemux86'

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
```

</span>

---

# **Build a target**

```bash
$ bitbake core-image-base
```
(video 4-first-build.sh)

- https://docs.yoctoproject.org/
- https://docs.yoctoproject.org/bitbake.html

---

# **Build a target**

<span style="font-size:60%">

```bash
yocto@yocto:~/co2-sensor/build$ bitbake core-image-base
Loading cache: 100% |                                                                  | ETA:  --:--:--
Loaded 0 entries from dependency cache.
Parsing recipes: 100% |################################################################| Time: 0:00:18
Parsing of 864 .bb files complete (0 cached, 864 parsed). 1507 targets, 70 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies

Build Configuration:
BB_VERSION           = "1.52.0"
BUILD_SYS            = "x86_64-linux"
NATIVELSBSTRING      = "ubuntu-20.04"
TARGET_SYS           = "arm-poky-linux-gnueabi"
MACHINE              = "raspberrypi3"
DISTRO               = "poky"
DISTRO_VERSION       = "3.4.2"
TUNE_FEATURES        = "arm vfp cortexa7 neon vfpv4 thumb callconvention-hard"
TARGET_FPU           = "hard"
meta                 
meta-poky            
meta-yocto-bsp       = "honister:0a26008bfc80f66cfab3d90c65b3e53d8cf12b88"
meta-raspberrypi     = "honister:378d4b6e7ba64b6a9a701457cc3780fa896ba5dc"

Initialising tasks: 100% |#############################################################| Time: 0:00:02
Sstate summary: Wanted 1625 Local 849 Network 0 Missed 776 Current 0 (52% match, 0% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 4040 tasks of which 1904 didn't need to be rerun and all succeeded.
```

</span>

---

# **Artefacts produced**

All in `build`:

- `downloads`: source packages
- `tmp/work` and `tmp/work-shared`: build directories
- `tmp/deploy/rpm`: individual packages
- `tmp/deploy/images`: flash images
- `sstate-cache`: cache of temporary artifacts

---

# **Generated image**

## In `build/tmp/deploy/images/raspberrypi3`:

- `core-image-base-raspberrypi3.tar.bz2`: root filesystem contents
- `core-image-base-raspberrypi3.ext3`: root filesystem image
- `core-image-base-raspberrypi3.manifest`: description of all packages installed in image
- `core-image-base-raspberrypi3.wic.bz2`: ready-to-flash SD card image

---

# **Flash the generated image**

```bash
$ sudo bmaptool copy core-image-base-raspberrypi3.wic.bz2 /dev/mmcblk0
bmaptool: info: discovered bmap file 'core-image-base-raspberrypi3.wic.bmap'
bmaptool: info: block map format version 2.0
bmaptool: info: 68915 blocks of size 4096 (269.2 MiB), mapped 38074 blocks (148.7 MiB or 55.2%)
bmaptool: info: copying image 'core-image-base-raspberrypi3.wic.bz2' to block device
'/dev/mmcblk0' using bmap file 'core-image-base-raspberrypi3.wic.bmap'
bmaptool: info: 100% copied
bmaptool: info: synchronizing '/dev/mmcblk0'
bmaptool: info: copying time: 31.6s, copying speed 4.7 MiB/sec
```

---

# **Run and test on Raspberry pi**

- Boot to a login prompt
- (root user, no password)
(video 6-boot.sh)

---

<style scoped> img[alt~="center"]{display: block; margin: auto; } </style>

# **Let's support our CO2 sensor**

![center](img/scd30.png)

https://sensirion.com/products/catalog/SCD30/

---

# **Supporting the SCD30**

Let's use a python library:

https://pypi.org/project/scd30-i2c/

It's not in openembedded.org layers!
We'll need to create our own recipe.

---

# **Where to place the new recipe?**

- We can't just modify existing layers
- Let's create our new layer

```bash
$ cd ~/co2-sensors
$ bitbake-layers create-layer meta-co2-sensor
NOTE: Starting bitbake server...
Add your new layer with 'bitbake-layers add-layer meta-co2-sensor'
$ bitbake-layers add-layer meta-co2-sensor
NOTE: Starting bitbake server...
```

---

# **We have a new layer**

```bash
$ git diff build/conf/bblayers.conf
```
```patch
diff --git a/build/conf/bblayers.conf b/build/conf/bblayers.conf
index 88087b3..d2d9cd7 100644
--- a/build/conf/bblayers.conf
+++ b/build/conf/bblayers.conf
@@ -10,4 +10,5 @@ BBLAYERS ?= " \
   /home/yocto/co2-sensor/poky/meta-poky \
   /home/yocto/co2-sensor/poky/meta-yocto-bsp \
   /home/yocto/co2-sensor/meta-raspberrypi \
+  /home/yocto/co2-sensor/meta-co2-sensor \
   "
```

---

# **We have a new layer**

```bash
meta-co2-sensor
├── conf
│   └── layer.conf
├── COPYING.MIT
├── README
└── recipes-example
    └── example
        └── example_0.1.bb

3 directories, 4 files
```

---

# **We have a new layer**

```bash
yocto@yocto:~/co2-sensor$ cat meta-co2-sensor/conf/layer.conf 
# We have a conf and classes directory, add to BBPATH
BBPATH .= ":${LAYERDIR}"

# We have recipes-* directories, add to BBFILES
BBFILES += "${LAYERDIR}/recipes-*/*/*.bb \
            ${LAYERDIR}/recipes-*/*/*.bbappend"

BBFILE_COLLECTIONS += "meta-co2-sensor"
BBFILE_PATTERN_meta-co2-sensor = "^${LAYERDIR}/"
BBFILE_PRIORITY_meta-co2-sensor = "6"

LAYERDEPENDS_meta-co2-sensor = "core"
LAYERSERIES_COMPAT_meta-co2-sensor = "honister"
```
---

# **We have a new layer**

- ... and an example recipe `example_0.1.bb`

<span style="font-size:60%">

```bash
yocto@yocto:~/co2-sensor$ cat meta-co2-sensor/recipes-example/example/example_0.1.bb 
SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe created by bitbake-layers"
LICENSE = "MIT"

python do_display_banner() {
    bb.plain("***********************************************");
    bb.plain("*                                             *");
    bb.plain("*  Example recipe created by bitbake-layers   *");
    bb.plain("*                                             *");
    bb.plain("***********************************************");
}

addtask display_banner before do_build
```
</span>

---

# **Create recipe python3-scd30**

```bash
$ cd meta-co2-sensor/
$ mkdir -p recipes-scd30/python3-scd30
$ cd recipes-scd30/python3-scd30
$ recipetool create -N python3-scd30 \
                    --autorev \
                    https://github.com/RequestForCoffee/scd30.git
...
...
NOTE: Tasks Summary: Attempted 2 tasks of which 0 didn't need to be rerun and all succeeded.
INFO: Scanning paths for packages & dependencies: .
INFO: Recipe python3-scd30_git.bb has been created; further editing may be required to make it fully functional
```

---

# **Create recipe python3-scd30**

`meta-co2-sensor/recipes-scd30/python3-scd30/python3-scd30_git.bb` (1/2):

<span style="font-size: 50%">

```bash
# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "SCD30 CO₂ sensor Python driver"
HOMEPAGE = "https://github.com/RequestForCoffee/scd30"
# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=61b6594a5230a3e37f4a47190fdd67d1"

SRC_URI = "git://github.com/RequestForCoffee/scd30.git;protocol=https;branch=master"

[...]
```

</span>

---

# **Create recipe python3-scd30**

`meta-co2-sensor/recipes-scd30/python3-scd30/python3-scd30_git.bb` (2/2):

<span style="font-size: 50%">

```bash
[...]

# Modify these as desired
PV = "0.0.5+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit setuptools3

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
RDEPENDS:${PN} += "python3-core python3-datetime python3-logging"

# WARNING: We were unable to map the following python package/module
# dependencies to the bitbake packages which include them:
#    smbus2
```

</span>

---

# **We need a recipe for smbus2**

https://layers.openembedded.org tells us `python3-smbus2` is in `meta-openembedded/meta-python`.

`0-bootstrap.sh`:
```bash
#!/bin/bash
BRANCH=honister

git clone -b $BRANCH git://git.yoctoproject.org/poky
git clone -b $BRANCH git://git.yoctoproject.org/meta-raspberrypi
git clone -b $BRANCH git://git.openembedded.org/meta-openembedded
```

---

# **We need a recipe for smbus2**

`build/conf/bblayers.conf`:
```bash
[...]
BBLAYERS ?= " \
  /home/yocto/co2-sensor/poky/meta \
  /home/yocto/co2-sensor/poky/meta-poky \
  /home/yocto/co2-sensor/poky/meta-yocto-bsp \
  /home/yocto/co2-sensor/meta-raspberrypi \
  /home/yocto/co2-sensor/meta-co2-sensor \
  /home/yocto/co2-sensor/meta-openembedded/meta-oe \
  /home/yocto/co2-sensor/meta-openembedded/meta-python \
  "
```

---

# **Now python3-scd30 depends from python3-smbus2**

`meta-co2-sensor/recipes-scd30/python3-scd30/python3-scd30_git.bb`:

<span style="font-size: 50%">

```bash
SUMMARY = "SCD30 CO₂ sensor Python driver"
HOMEPAGE = "https://github.com/RequestForCoffee/scd30"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=61b6594a5230a3e37f4a47190fdd67d1"

SRC_URI = "git://github.com/RequestForCoffee/scd30.git;protocol=https;branch=master"

PV = "0.0.5+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit setuptools3

RDEPENDS:${PN} += "python3-core python3-datetime python3-logging"
RDEPENDS:${PN} += "python3-smbus2"
```
</span>

---

# **Test building python3-scd30**

We don't have to build full images,
we can build single packages as well: 

```bash
$ bitbake python3-scd30
[...]
NOTE: Tasks Summary: Attempted 899 tasks of which 894 didn't need to be rerun and all succeeded.
```

---

# **Let's ship python3-scd30 in the image**

- `core-image-base` is defined in `poky`
- Again, we don't want to modify `poky` ! (very bad idea)
- Let's declare an **extension** to `core-image-base` in our new layer `meta-co2-sensor`.
- This extension will include this package (and dependencies).

---

# **Add python3-scd30 to image**

```bash
yocto@yocto:~/co2-sensor$ recipetool newappend -w meta-co2-sensor core-image-base
NOTE: Starting bitbake server...
Loading cache: 100% |###############################################| Time: 0:00:00
Loaded 1508 entries from dependency cache.
/home/yocto/co2-sensor/meta-co2-sensor/recipes-core/images/core-image-base.bbappend
```

`meta-co2-sensor/recipes-core/images/core-image-base.bbappend` is created empty.
We write this declaration into it:
```
IMAGE_INSTALL:append = "python3-scd30"
```

---

# **Rebuild the image**

```bash
$ bitbake core-image-base
[...]
NOTE: Tasks Summary: Attempted 4066 tasks of which 4055 didn't need to be rerun and all succeeded.
```

---

# **Test on the target**

`test_scd30.py`:

<span style="font-size:60%">

```python
import time
from scd30_i2c import SCD30

scd30 = SCD30()

scd30.set_measurement_interval(2)
scd30.start_periodic_measurement()

time.sleep(2)

while True:
    if scd30.get_data_ready():
        m = scd30.read_measurement()
        if m is not None:
            print(f"CO2: {m[0]:.2f}ppm, temp: {m[1]:.2f}'C, rh: {m[2]:.2f}%")
        time.sleep(2)
    else:
        time.sleep(0.2)
```

</span>

---

# **Test on the target**

```bash
$ python3 test_scd30.py
CO2: 785.31ppm, temp: 30.51'C, rh: 23.04%
CO2: 787.77ppm, temp: 30.51'C, rh: 23.09%
CO2: 780.87ppm, temp: 30.51'C, rh: 23.11%
CO2: 779.52ppm, temp: 30.50'C, rh: 23.14%
CO2: 774.22ppm, temp: 30.50'C, rh: 23.20%
CO2: 783.18ppm, temp: 30.48'C, rh: 23.21%
CO2: 784.75ppm, temp: 30.48'C, rh: 23.21%
CO2: 776.97ppm, temp: 30.51'C, rh: 23.18%
CO2: 788.11ppm, temp: 30.51'C, rh: 23.40%
CO2: 800.32ppm, temp: 30.50'C, rh: 23.77%
CO2: 810.02ppm, temp: 30.46'C, rh: 23.95%
```

---

# **Some more software: sensor server**

- Now, let's add a real service
- Read data from I2C, and send it to MQTT.
- Create a recipe in `meta-co2-sensor/recipes-scd30`:

```bash
$ tree meta-co2-sensor/recipes-scd30/python3-scd30-mqtt-bridge
meta-co2-sensor/recipes-scd30/python3-scd30-mqtt-bridge
├── python3-scd30-mqtt-bridge
│   ├── LICENSE
│   ├── scd30_mqtt_bridge.py
│   └── scd30-mqtt-bridge.service
└── python3-scd30-mqtt-bridge_1.0.bb
1 directory, 4 files
```

---

# **Some more software: sensor server**

<span style="font-size:70%">

`python3-scd30-mqtt-bridge_1.0.bb` (1/3):
```bash
SUMMARY = "SCD30 CO₂ sensor MQTT bridge"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=61b6594a5230a3e37f4a47190fdd67d1"

SRC_URI = " \
    file://scd30_mqtt_bridge.py \
    file://scd30-mqtt-bridge.service \
    file://LICENSE \
"

[...]
```
</span>

---

# **Some more software: sensor server**

<span style="font-size:70%">

`python3-scd30-mqtt-bridge_1.0.bb` (2/3):
```bash
[...]

S = "${WORKDIR}"

inherit allarch python3-dir systemd

RDEPENDS:${PN} += "python3 python3-scd30 python3-paho-mqtt"

SYSTEMD_SERVICE:${PN} = "scd30-mqtt-bridge.service"

[...]
```

`allarch`, `python3-dir` and `systemd` are bbclasses
</span>

---

# **Some more software: sensor server**

<span style="font-size:70%">

`python3-scd30-mqtt-bridge_1.0.bb` (3/3):
```bash
[...]

do_install:append() {
        install -d ${D}${PYTHON_SITEPACKAGES_DIR}
        install -m 0755 ${WORKDIR}/scd30_mqtt_bridge.py ${D}${PYTHON_SITEPACKAGES_DIR}

        install -d -m 0755 ${D}${systemd_unitdir}/system
        install    -m 0644 ${S}/scd30-mqtt-bridge.service ${D}${systemd_unitdir}/system
}

FILES:${PN} += "${PYTHON_SITEPACKAGES_DIR}/scd30_mqtt_bridge.py"
```
</span>

---

# **Some more software: sensor server**

Make sure this service is part of the image:

`meta-co2-sensor/recipes-core/images/core-image-base.bbappend`:

```bash
IMAGE_INSTALL:append = "python3-scd30-mqtt-bridge"
```

We can remove `python3-scd30`, it will be included as a dependency. 

---

<style scoped> img[alt~="center"]{display: block; margin: auto; } </style>

# **Results posted on a server**

![w:1050 center](img/grafana.png)

---

# **Takeaway: Yocto is…**

- very powerful and flexible
- very modular
- complex
- well-documented
- aimed at complex products

---

# **More info: references**

- https://yoctoproject.org
- https://layers.openembedded.org
- https://sensirion.com/products/catalog/SCD30/
- https://pypi.org/project/scd30-i2c/
- https://github.com/RequestForCoffee/scd30

---

# **More info: references**

- https://bootlin.com/training/
- https://embeddedlinuxconference.com/

---

# **If you think you don't have any questions… think harder.**

https://docs.yoctoproject.org/current/what-i-wish-id-known.html

---

CO2 sensor project sources:
https://gitlab.com/lacoute974/iot/co2-sensor

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

<span style="font-size:60%">

- Matrix:  @lacouture:matrix.org
- Twitter: @PatLacouture
- LinkedIn: Patrice LACOUTURE

</span>

---

# **Credits**

<span style="font-size:60%">

https://commons.wikimedia.org/w/index.php?curid=113003174
https://commons.wikimedia.org/w/index.php?curid=66907986
https://commons.wikimedia.org/w/index.php?curid=1640046
https://commons.wikimedia.org/w/index.php?curid=38178282
https://commons.wikimedia.org/w/index.php?curid=500910
https://commons.wikimedia.org/w/index.php?curid=4488022
https://en.wikipedia.org/w/index.php?curid=25494818
https://commons.wikimedia.org/w/index.php?curid=18296544
https://commons.wikimedia.org/w/index.php?curid=3330975
https://commons.wikimedia.org/w/index.php?curid=45217995

</span>
