#!/bin/bash
SOURCE=lacouture-introduction-to-yocto.md

marp --html $SOURCE
marp --html --pdf --allow-local-files $SOURCE
