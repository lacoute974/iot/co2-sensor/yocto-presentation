Introduction to Yocto
====

Create your own Linux distribution for embedded devices.

by Patrice Lacouture

Purpose
----

This document serves as support material for a 1h30 presentation that introduces base concepts
of Yocto to developers.

As a demonstration/exercise, we create a Linux image for a CO2 / Relative Humidity / Temperature sensor
based on a Raspberry Pi 3 B.

Format and usage
----

This presentation is designed with the Marp tool: https://marp.app/

It's been generated using this version:

```
@marp-team/marp-cli v1.7.0 (w/ @marp-team/marp-core v2.3.2)
```

To build a HTML and a PDF presentation, run the `build.sh` script.

The `vid` directory contains recordings of terminal interactions a various stages of the presentation.
These recording have been made with `asciinema` 2.1.0: https://asciinema.org

To play them, run the shell scripts present in the `vid` directory.
